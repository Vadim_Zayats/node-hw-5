import { FileDB } from "./service/crud-db-service";
import { InputData } from "./types/types";

export const fileDB = FileDB.getInstance();

export const newspostTable = fileDB.getTable("newsposts");

export const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

export const data0: InputData = {
  title: "Title 0",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem?",
};
export const data1: InputData = {
  title: "Title 1",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id nece.",
};
export const data2: InputData = {
  title: "Title 2",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing iusto voluptate expedita cumque minus odit ea autem cusamus id necessitatibus iusto voluptate expedita cumque minus?",
};
export const data3: InputData = {
  title: "Title 3",
  text: "Lorem ipsum dolor sit amet accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem.",
};

(async () => {
  fileDB.registerSchema("newsposts", newspostSchema);
  await newspostTable.create(data0);
  await newspostTable.create(data1);
  await newspostTable.create(data2);
  await newspostTable.create(data3);
})();
